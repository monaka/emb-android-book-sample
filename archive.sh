#!/bin/bash

TAG=20121018

rm -fr EmbAndroid


for i in 3; do
	git archive --format tar --prefix=EmbAndroid/Part1/ chap${i}_$TAG -- hello-world | tar xf -; mv EmbAndroid/Part1/hello-world EmbAndroid/Part1/Chap$i
done

for i in 4-2 4-3 4-4 5 6 7 8; do
	git archive --format tar --prefix=EmbAndroid/Part2/ chap${i}_$TAG -- org.monaka.book.android.camera.first | tar xf -; mv EmbAndroid/Part2/org.monaka.book.android.camera.first EmbAndroid/Part2/Chap$i
done

for i in 9 10 11 12; do
	git archive --format tar --prefix=EmbAndroid/Part3/ chap${i}_$TAG -- org.monaka.book.android.camera.first | tar xf -; mv EmbAndroid/Part3/org.monaka.book.android.camera.first EmbAndroid/Part3/Chap$i
done

